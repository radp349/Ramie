## Projekt Java3D

### Opis projektu
Celem projektu było zrealizowanie animowanej wizualizacji ramienia robota typu "articulated arm" przy użyciu technologii Java3D. Aplikacja umożliwia użytkownikowi kontrolę nad ruchem ramienia robota w trzech stopniach swobody, robotaniec oraz wykorzystanie kinematyki odwrotnej.

### Osiągnięcia projektowe
W ramach projektu udało się zrealizować szereg ważnych funkcjonalności:
- Program został podzielony na kilka klas, co przyczyniło się do czytelnego i przejrzystego kodu.
- Umożliwiono użytkownikowi ruch kamery w celu swobodnego poruszania się po scenie i zmiany perspektywy.
- Stworzono estetyczne tło z nałożonymi teksturami w formie "cubemap", które nadaje scenie bardziej realistyczny wygląd.
- Dodano model ramienia robota oraz możliwość sterowania nim za pomocą klawiszy Q, E, W, S, A i D.
- Implementowano przycisk Resetu Kamery, który przywraca początkowe ustawienia kamery.
- Nagrywanie i odtwarzanie ruchu robota.
- Moliwość interakcji z piłką w tym upuszczania jej.
- **Opcja Robotańca**
- **Wyświetlania aktualnej pozycji manipulatora przy zachowaniu wielowątkowości.**
- **Została zrealizowana kinematyka odwrotna (RRR)**

![Wideo podglądowe](Robotaniec_muzyka.mp4)
